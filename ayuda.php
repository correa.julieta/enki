<!DOCTYPE html>
<html>
<head>
	<title>Ayuda - Enki</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Productos</title>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="fonts/style.css">
	<link rel="stylesheet"  href="css/footer.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<link rel="stylesheet" type="text/css" href="css/ayuda.css">
</head>
<body>


 <?php include("includes/inicio.php");
			 include("includes/main.html");?>

<div class="p">


<div class="titulo">
	<h1>Preguntas Frecuentes|Ayuda</h1>
</div>



 <div class="preg">
 	 	<details>
 		<summary>¿Que es Enki?</summary>
 		<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 		tempor incididunt ut labore et dolore magna aliqua.</a>
 	</details>
 	<details>
 		<summary>¿Cuales son los medios de pago?</summary>
 		<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 		tempor incididunt ut labore et dolore magna aliqua.</a>
 	</details>
 	<details>
 		<summary>¿Como vender?</summary>
 		<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 		tempor incididunt ut labore et dolore magna aliqua.</a>
 	</details>
 	<details>
 		<summary>¿Como editar una publicacion?</summary>
 		<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 		tempor incididunt ut labore et dolore magna aliqua.</a>
 	</details>
 </div>


</div> <?php include("includes/footer.html"); ?>

<script src="js/main.js"></script>
</body>
</html>