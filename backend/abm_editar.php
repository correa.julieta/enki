<?php require_once("conexion.php");


include("redimensionaImagen.php");

$id_editar=$_REQUEST['id_editar'];
$sql="SELECT * FROM productos WHERE id_producto=$id_editar";
$consulta=mysqli_query($conexion, $sql);
$registro=mysqli_fetch_assoc($consulta);
?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	
	<link rel="stylesheet" href="css/general.css">
	<link rel="stylesheet" href="css/abm_editar.css">
	
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<title>Editar Producto</title>
</head>
<body>

		<article>
			<header class="cabecera">
				<?php include("includes/header.php");?>
			</header>

	<header class="cabecerares">
		<?php include("includes/menures.php");?>
	</header>

		<main>

		<div class="editaprod">
			<div class="editatitufoto">
			<p>Editar Producto</p>
				<div class="fotoprod">
				<?php 
				if (!empty($registro['imagen_producto'])) {
					echo '<img src="img/productos/'.$registro['imagen_producto'].'">';
				}else{ 
					echo '';}
				 ?>
				 </div></div>

		<form method="POST" action="abm_editar.php" enctype="multipart/form-data">
			

	<input type="file" name="foto" class="subirimagen">
	<input type="hidden" name="foto_previa" value="<?php echo $registro['imagen_producto'];?>">
	<input type="hidden" name="id_editar" value="<?php echo $registro['id_producto'];?>">

	<label>Nombre del Producto</label>
	<input type="text" name="nombre_prod" value="<?php echo $registro['nombre_producto']; ?>" autocomplete="off">

	<label>Precio del Producto</label>
	<input type="number" name="precio_prod"  value="<?php echo $registro['precio_producto']; ?>">

	<label>Etiquetas</label>
	<input type="text" name="tags_prod"  value="<?php echo $registro['tags_producto']; ?>">

	<select name="categoria">
		<?php
		switch ($registro['categoria_producto']) {
			case 'Perfumeria':
			echo '<option value="Perfumeria" > Perfumeria	</option>
					<option value="Juguetes"> Juguetes	 </option>
					<option value="Comida"> Comida </option>
					<option value="Limpieza"  > Limpieza </option>';
					break;
			case 'Juguetes':
			echo '<option value="Perfumeria"> Perfumeria	</option>
					<option value="Juguetes" selected> Juguetes	 </option>
					<option value="Comida"> Comida </option>
					<option value="Limpieza"  > Limpieza </option>';
					break;
			case 'Comida':
			echo '<option value="Perfumeria" > Perfumeria	</option>
					<option value="Juguetes"> Juguetes	 </option>
					<option value="Comida" selected> Comida </option>
					<option value="Limpieza"  > Limpieza </option>';
					break;
			case 'Limpieza':
			echo '<option value="Perfumeria" > Perfumeria	</option>
					<option value="Juguetes"> Juguetes	 </option>
					<option value="Comida"> Comida </option>
					<option value="Limpieza" selected> Limpieza </option>';
					break;}
	?>
</select>

<input type="submit" name="editar" value="Editar">
<input type="submit" name="cancelar" value="Cancelar" class="cancelar"></div>
</form>

		
</main>
		<footer class="pie">
			<?php include("includes/footer.php");?>
		</footer>
</article>
</body>
</html>

<?php

if(isset($_REQUEST['editar']))
{
	$id_editar=$_REQUEST['id_editar'];
	$foto_previa=$_REQUEST['foto_previa'];
	$nombre=$_REQUEST['nombre_prod'];
	$precio=$_REQUEST['precio_prod'];
	$categoria=$_REQUEST['categoria'];
	$tags=$_REQUEST['tags_prod'];

	if(is_uploaded_file($_FILES['foto']['tmp_name']))

	{
		$fotosubi=$_FILES['foto']['name'];
		move_uploaded_file($_FILES['foto']['tmp_name'], $fotosubi);

		$nombreImg=redimensionarImagen($fotosubi, 500,500); unlink($fotosubi);
	} else{
		$nombreImg=$foto_previa;

	}

	$sql="UPDATE productos SET imagen_producto = '$nombreImg', nombre_producto='$nombre', precio_producto='$precio', categoria_producto='$categoria', tags_producto='$tags' WHERE id_producto = '$id_editar'";

$editar=mysqli_query($conexion, $sql)? print('<script>document.location.replace("abm.php?id_modificado='.$id_editar.'");</script>') : print('<script> alert("Error al Modificar Producto")</script>');
}

if(isset($_REQUEST['cancelar']))
{
echo "<script>document.location.replace('abm.php');</script>";
}
?>