<?php 
if (isset($_GET['categoria'])) {
	echo '<p class="Subtitulo"> Productos: '.$_GET['categoria'].'</p>';
}
require_once("conn.php");
require_once("filtros.php");
if (mysqli_num_rows($consulta)>0) {
 	while ($registro=mysqli_fetch_assoc($consulta)) {

 	?>

<details>
			<summary>
				<p class="nbr_prod">
				<?php 
				if (empty($registro['foto'])) {
				 	echo '<img src="../imagenes/descarga.png">';
				 } else{
				 	echo '<img src="../imagenes/'.$registro['foto'].'">';
				 }
				 ?>
					<?php echo $registro['nbr_prod']; ?></p></summary>
			
			<p class="items">Descripcion: <?php echo $registro['descripcion']; ?> </p>
			<p class="items">Precio: $<?php echo $registro['precio']; ?></p>
			<p class="items">Cantidad: <?php echo $registro['cantidad']; ?></p>
			<p class="items">Categoria: <?php echo $registro['categoria']; ?></p>
			<p class="items">Envio: <?php echo $registro['envio']; ?></p>
			<a href="abm.php?id_editar=<?php echo $registro['id_producto']?>" onclick="return confirm('Confirma que desea editar')"><span class="icon-pencil"></span></a>
			<a href="abm.php?id_borrar=<?php echo $registro['id_producto']?>" onclick="return confirm('Confirma que desea borrar')"><span>Borrar</span></a>		
</details> 
<?php
} 
} else {
 	echo "No hay productos";
 }
  ?>