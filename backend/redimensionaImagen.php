<?php

function redimensionarImagen($imagen, $ancho_final, $alto_final) {

	list($ancho_orig, $alto_orig, $nroTipo) = getimagesize($imagen);
	switch ($nroTipo) {

		case 2: $img_original=imagecreatefromjpeg($imagen);
		break;
		case 3: $img_original=imagecreatefrompng($imagen);
		break;
		default: echo "Tipo de archivo incorrecto";
	}
	$ratio_ancho= $ancho_final/ $ancho_orig;
	$ratio_alto= $alto_final/ $alto_orig;
	
	$ratioMax= max($ratio_ancho, $ratio_alto);

	$nuevoancho= $ancho_orig*$ratioMax;
	$nuevoalto= $alto_orig*$ratioMax;


	$cortarAncho=$nuevoancho-$ancho_final;
	$cortarAlto=$nuevoalto-$alto_final;

	$desplazamiento= 0.5;


	$nueva_img = imagecreatetruecolor($ancho_final, $alto_final);

	imagecopyresampled($nueva_img, $img_original, -$cortarAncho*$desplazamiento,-$cortarAlto*$desplazamiento,0,0, $ancho_final+$cortarAncho, $alto_final + $cortarAlto, $ancho_orig, $alto_orig);

	imagedestroy($img_original);

	$calidad = 70;

	$nombre_imagen = time()."-".$imagen;

	imagejpeg($nueva_img, "../imagenes/$nombre_imagen", $calidad);

	return $nombre_imagen;

}

?>