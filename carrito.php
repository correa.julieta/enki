<!DOCTYPE html>
<html>
<head>
	<title>Carrito - Enki</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="fonts/style.css">
	<link rel="stylesheet"  href="css/footer.css">
	<link rel="stylesheet"  href="css/barra-social.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<link rel="stylesheet" type="text/css" href="css/carrito.css">
</head>
<body>
 <?php include("includes/main.html"); ?>
<div class="header">
		<div class="primerp"><p>PASO UAN </p></div>
		<div class="segundop"><p>PASO TU </p></div>
		<div class="tererp"><p>PASO TRI </p></div>
	</div>

<div class="padre">

	
	<div class="primerisimo">
<div class="titulouno"><h2>Carrito</h2></div><hr class="sect">


	
	<div class="sect">
		<div class="img"><img src="imagenes/descarga.png"></div>
		<div class="inf">
			<p>[Nbr producto]</p>
			<p>[desc producto]</p>
			<p>[precio producto]</p>	
		</div>
		<div class="mm">
			<div class="cant"> <input type="number" name="cantidad" min="1" max="20" value="1"></div>
		</div>
	</div>







<hr class="sect">
<div class="botones">
			<button type="submit" name="siguiente">Siguiente</button> <button type="submit" name="cancelar">Cancelar</button>
		</div>
	</div>
		<div class="segundisimo">
	<div class="resumen">

		<div class="titulodos">
			<h2>Resumen</h2>
		</div>

		<hr class="resumen">

		<div class="cod">[Ingrese Cod]</div>
		<hr class="resume">






		<div class="doz">
		<div class="infgen">
			<p>Subtotal</p> 
			<p>Envio</p>
			<p>Comisión</p>
		</div>
		
		<div class="infpedido">
			<p>[precio sub]</p>
			<p>[precio envio]</p>
			<p>[precio comision]</p>
		</div>
	</div>



		<hr class="resumen">

		<div class="total">
			<div class="titulotres"><p>TOTAL</p></div>
		
		<div class="preciof"><p>[precio final]</p></div>
		</div></div>
	</div>

</div>
</div>



 <?php include("includes/footer.html"); ?>
</body>
</html>