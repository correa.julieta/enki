<!DOCTYPE html>
<html>
<head><meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Enki</title>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="fonts/style.css">
	<link rel="stylesheet"  href="css/categorias.css">
<link rel="stylesheet"  href="css/footer.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<title></title>
</head>
<body>
<div class="contenedor">
	 <?php 
	 include("includes/inicio.php");
	 include("includes/main.html"); ?>
	 <section class="contenido">
	 	<div class="categorias">
	 	<a href="productos.php?categoria=accesorios"><img src="imagenes/belleza.fw.png"></a>
	 	<a href="productos.php?categoria=alimentos"><img src="imagenes/alimentos.fw.png"></a>
	 	<a href="productos.php?categoria=bebes"><img src="imagenes/bebes.fw.png"></a>
	 	<a href="productos.php?categoria=belleza"><img src="imagenes/beleza.fw.png"></a>
	 	<a href="productos.php?categoria=energia"><img src="imagenes/energia.fw.png"></a>
	 	<a href="productos.php?categoria=decoracion"><img src="imagenes/deco.fw.png"></a>
	 	<a href="productos.php?categoria=hogar"><img src="imagenes/hh.fw.png"></a>
	 	<a href="productos.php?categoria=juguetes"><img src="imagenes/juegos.fw.png"></a>
	 	<a href="productos.php?categoria=mascotas"><img src="imagenes/mascotas.fw.png"></a>
	 	<a href="productos.php?categoria=plantas"><img src="imagenes/plantas.fw.png"></a>
	 	<a href="productos.php?categoria=ropa"><img src="imagenes/ropa.fw.png"></a>
	 	<a href="productos.php?categoria=salud"><img src="imagenes/salud.fw.png"></a>
	 
	 
	 	
	 </div>
	 </section>
	
<?php include("includes/footer.html"); ?>
</div>
<script src="js/main.js"></script>
</body>
</html>