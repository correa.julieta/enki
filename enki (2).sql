-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-10-2019 a las 05:31:53
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `enki`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nbr_prod` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `precio` int(40) NOT NULL,
  `cantidad` int(40) NOT NULL,
  `categoria` text COLLATE utf8_spanish_ci NOT NULL,
  `envio` text COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nbr_prod`, `descripcion`, `precio`, `cantidad`, `categoria`, `envio`, `foto`, `id_usuario`) VALUES
(1, 'Cartera de corcho', 'Casfhsal', 250, 0, 'accesorios', 'gratis', '1568742731-81XF1RJTkaL._SL1500_-300x234.jpeg', 12),
(4, 'Shampu ', 'Shampu ecologico, no testeado en animales', 132, 15, 'belleza', 'sin', '1568743502-champu-gel-ecologico-300x273.jpg', 0),
(6, 'Mechero solar', 'Ideal para llevar a la cancha, de camping, a la playa y supervivencia.', 220, 10, 'herramienta', 'gratis', '1568744037-mechero-solar-300x300.jpeg', 0),
(15, 'Labial dorado', 'Luci hermosa y cuida el planeta con los nuevos labiales ecologiocos', 150, 7, 'belleza', 'pago', '1571705959-3d066b_CSV_396_491_R.jpg', 0),
(17, 'Espejo con cubierta de bambu', 'Espejos copados, artesanales y ecolÃ³gicos!', 90, 20, 'accesorios', '', '1571706269-5ce98e_CSV_254.jpg', 0),
(18, 'Peluche para mascotas', 'Peluches para mascotas hecho atraves de materiales reutilizados', 290, 5, 'mascotas', '', '', 12),
(19, 'Peluche para mascotas', 'Peluches para mascotas hecho atraves de materiales reutilizados', 290, 5, 'mascotas', '', '', 12),
(21, 'Espaguetis sabor algas', 'Hecho a base de algas deshidratadas', 80, 4, 'alimentos', '', '1571711882-algas-espagueti-deshidratada-ecologica-650x650-300x300.jpg', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nbr_usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `email_usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `tfn_usuario` int(25) NOT NULL,
  `direc_usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `pass_usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `activacion` text COLLATE utf8_spanish_ci NOT NULL,
  `foto_usuario` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nbr_usuario`, `email_usuario`, `tfn_usuario`, `direc_usuario`, `pass_usuario`, `activacion`, `foto_usuario`) VALUES
(1, 'Juan Ariel', 'juanceto.tubebe@gmail.com', 1011254564, 'av. siempre viva 200', 'contraseña', '', 'bebe.jpg'),
(2, 'Celeste Anahi', 'laceletuamor@hotmail.com', 88045512, 'Marconi 432', 'demilovatomimujer', '', 'demi.jpg'),
(3, 'Baekhyun', 'diosodediosas@gmail.com', 45126654, 'Corea del sur', 'chanbeakforeva', '', 'bb.jpg'),
(4, 'julieta', 'correajuli@hotmail.com', 0, '', 'pepe', '7f89920b8e878f8d7bcebd9ab47f6a37', ''),
(8, 'julieta', 'correajuli@hotmail.com', 0, '', 'pepe', 'be6d0425b38d23dc7fe573ce8834f627', ''),
(9, 'wedr', 'correai@hotmail.com', 3432, 'Darwin 796', 'hola', '9575d27716925b65583e42c494d0172b', '1571709918-fotonoticia_20161023074138_640.jpg'),
(10, 'juan', 'correajuli@hotmail.com', 0, '', 'hola', '17e9da5576e4116de2dee32cb2774877', ''),
(12, '', 'josesito@gmail.com', 2434, 'Darwin 796', 'pepe', '5624636fcd48f2c264d8b9e1fb4f4b86', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_venta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `id_producto`, `id_usuario`, `fecha_venta`) VALUES
(1, 3, 12, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
