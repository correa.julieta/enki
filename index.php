<?php 
require_once("./backend/conn.php");

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Enki</title>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="fonts/style.css">
	<link rel="stylesheet"  href="css/index.css">
	<link rel="stylesheet"  href="css/footer.css">

	
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	
</head>
<body>
	<div class="contenedor"> <?php  
	include("includes/inicio.php");
			 include("includes/main.html");
			 include("backend/filtrosCategorias.php") ?>
	<div class="imagen">
		<div class="banner">
			<h1>Enki-Commerce</h1>
			<p>No habia presupuesto para nuestro slogan</p>	
		</div>
			<form class="headerForm">
				<input type="search" id="buscador-index" placeholder="Buscar">
				<button type="submit" class="btn-busc-index">
					<i class="icon-search"></i>
				</button>
			</form>
	 	</div>
	 </div>
	
  <section>
	<article class="categorias">	<h2>Categorias destacadas</h2>
	 <div class="catedest">
	  <div class="cate cate1"><a href="hogar.php"><div class="imagen-cat imgcat1"><div class="hover-ca"><p>Comprar ahora</p></div></div> </a></div>
	  <div class="cate cate2"><a href="hogar.php"><div class="imagen-cat imgcat2"><div class="hover-ca"><p>Comprar ahora</p></div></div> </a></div>
	  <div class="cate cate3"><a href="hogar.php"><div class="imagen-cat imgcat3"><div class="hover-ca"><p>Comprar ahora</p></div></div></a> </div>
	  <div class="cate cate4"><a href="hogar.php"><div class="imagen-cat imgcat4"><div class="hover-ca"><p>Comprar ahora</p></div></div> </a></div>
	 </div>
   </article>
 </section>
 <section class="nove_prod">
 	<article class="novedades">
	 <h2>Novedades</h2>
	 <form>
	  <input type="text" id="email" placeholder="Email">
	  <input type="submit" id="suscribir" value="Suscribirse">
     </form>
    </article>
    <aside>
	  <?php
		$sql1="SELECT * FROM productos WHERE precio=250 ";
$consulta1=mysqli_query($conexion,$sql1);
	while($registro1=mysqli_fetch_assoc($consulta1)){
	
				if (empty($registro1['foto'])) {
				 	echo '<img src="imagenes/descarga.png">';
				 } else{
				 	echo '<img src="imagenes/'.$registro1['foto'].'">';
				 }
				 ?>
	  <h2 class="prodDes">Producto destacado</h2>
	  <p><?php echo $registro1['nbr_prod'] ; ?></p>
	  <p><?php echo $registro1['descripcion'] ; }?></p>
	  <button id="ver_mas">ver mas</button>
    </aside>
    <article class="productos"><?php
		$sql="SELECT * FROM productos LIMIT 4";
$consulta=mysqli_query($conexion,$sql);
	while($registro=mysqli_fetch_assoc($consulta)){
		?>
	 <div class="producto"><a href="">
		<?php 
				if (empty($registro['foto'])) {
				 	echo '<img src="imagenes/descarga.png">';
				 } else{
				 	echo '<img src="imagenes/'.$registro['foto'].'">';
				 }
				 ?>
		 <h2><?php echo $registro['nbr_prod'] ; ?></h2>
		 <p>[estrellas?]</p>
		 <p>$<?php echo $registro['precio'] ; ?></p></a>
	 </div>
	 <?php 
}

?> 
    </article>
   </section>
 <?php include("includes/footer.html"); ?>
</div>
<script src="js/main.js"></script>

</body>
</html>