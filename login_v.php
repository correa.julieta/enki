<?php 
	require_once('./backend/conn.php');
	session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login/Register</title>
	<meta name="viewport" content="widt=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="./css/login.css">
</head>
<body>
<div class="cont_f">
	<div class="header_f">
		<div class="logo">	
			<h1>OLAS</h1>
		</div>
		<div class="menu">
			<a href="login_v.php"><li class="mod_login active">Login</li></a>
			<a href="registrar_v.php"><li class="mod_reg active">Registrar</li></a>
		</div>
	</div>
<form action="login_v.php" method="POST" class="form">
	<div class="bien">
		<h2>Bienvenido</h2>
	</div>

	<div class="user line-input">
		<label class="lnr lnr-user"></label>
		<?php 
			if (!isset($_COOKIE['user'])) {
				echo '<input type="text" name="usuario" placeholder="Ingresa nombre...">';
			}else{
				echo '<input type="text" name="usuario" value="'.$_COOKIE['user'].'">';
			}
		 ?>
	</div>
	<div class="cont line-input">
		<label class="lnr lnr-lock"></label>
		<input type="password" name="clave" placeholder="Contraseña...">
	</div>
	<button type="submit" name="login">Iniciar sesion</button>
<div class="check">
	<label><input type="checkbox" name="recordarme"> Recordar mis datos</label>
	<a href="restablecer.php">¿Olvidaste tu contraseña?</a>
</div>
</form>
</div>
</body>
</html>
<?php 

if (isset($_POST["login"])) {
	$usuario=$_POST['usuario'];
	$pass=$_POST['clave'];
	$_SESSION['nbr_usuario']=$usuario;
	$sql="SELECT * FROM usuarios WHERE nbr_usuario='$usuario' AND pass_usuario='$pass'";
	
	if(isset($_POST['recordarme']) && !empty($_POST['recordarme'])){
		setcookie("user", $usuario, time()+3600, "/"); 
	}else{
		setcookie("user", $usuario, time()-1, "/"); 
	}
	$query=mysqli_query($conexion,$sql);
	$result=mysqli_num_rows($query);

	if ($result>0) {
		header('location:perfol.php');
	}
else{
	echo "<script>alert('usuario o clave erroneos')</script>";
	session_destroy();
}
}
?>
