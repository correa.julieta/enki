<?php


  require_once 'backend/conn.php';
  $q_last = mysqli_query($conexion,'SELECT * FROM productos ORDER BY id_producto DESC LIMIT 6');
  $q_count = mysqli_num_rows($q_last);

?>

<section class="bg-white" id="last-products">
  <div class="container overflow-hidden padding-vertical-hard-m">

    <h2 class="text-center lang" key="releases">
    Lanzamientos
    </h2>

    <div class="separator"></div>

    <div class="row text-left">
      <div class="full-width flex wrap justify-space-between">
        <div class="owl-carousel owl-theme">



<?php
  if($q_count >= 1){
    while($data=mysqli_fetch_assoc($q_last)){
?>

          <article class="item margin-top-hard-m padding-horizontal-hard-m margin-bottom-hard-m">
            <div class="row">
              <div class="col-md-6">
                <img src="assets/img/<?php

                    if(isset($data['foto']) && $data['foto'] != ''){
                      echo "imagenes/" . $data['foto'];
                    }else{
                      echo "no-img.png";
                    }

                    ?>" class="img-responsive">
              </div>
              <div class="col-md-6">
                <h4>
                  <?php
                    echo $data['nbr_prod'];
                  ?>
                </h4>
                <p class="small">
                  <?php
                    $length = strlen($data['precio']);
                    if($length > 40){
                      echo substr($data['precio'], 0, 40) . "...";
                    }
                    else{
                      echo $data['precio'];
                    }
                  ?>
                </p>
              </div>
              <br>
              <div class="col-md-12">
                <p class="text-right text-left-m margin-right-hard view-more">
                  <a href="producto.php?code=<?php echo $data['id_producto']; ?>" class="no-hover lang" key="more"></a>
                </p>
              </div>
            </div>
          </article>

<?php
  }
}
?>




        </div>
      </div>
    </div> <!-- Row -->

  </div>
</section>
