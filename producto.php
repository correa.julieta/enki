
</body>
</html><!DOCTYPE html>
<html>
<head>
	<title>[nbr producto]</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Productos</title>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="fonts/style.css">
	<link rel="stylesheet"  href="css/footer.css">
	<link rel="stylesheet" type="text/css" href="css/sugerencias.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<link rel="stylesheet" type="text/css" href="css/descProd.css">
</head>
<body>
<?php 
include("includes/inicio.php");
include("includes/main.html");
 include("prod-back.php") ;?>

<div class="cont-prod">
 
<div class="product">
	<div class="product-small-img" onclick="myFunction(this)">
		<img src="imagenes/img1.jpg" onclick="myFunction(this)">
		<img src="imagenes/img2.jpg" onclick="myFunction(this)">
		<img src="imagenes/img3.jpg" onclick="myFunction(this)">
	</div>
<?php

	 if (mysqli_num_rows($prod)>0) {
 	while ($registroP=mysqli_fetch_assoc($prod)) {?>
	<div class="img-container">
		<?php
				if (empty($registroP['foto'])) {
				 	echo '<img id="imageBox" src="imagenes/bb.jpg">';
				 } else{
				 	echo '<img  id="imageBox" src="imagenes/'.$registroP['foto'].'">';
				 }?>
	</div>
</div>

<div class="descripcion">

	<div class="titulo">
		<h1><?php echo $registroP['nbr_prod']; ?></h1>
	</div>

	<div class="estrellas"><span class="icon-heart"></span></div>

<div class="prsl">
	<div class="precio">
		<p>$ <?php echo $registroP['precio']; ?></p>
	</div>
	<div class="select">
		<select name="op">
			<option>Elige una opcion</option>
		</select>
	</div>
</div>


	<div class="desc">
		<p> <?php echo $registroP['descripcion']; ?></p>
	</div>

	<div class="boton">
		<button type="submit" name="agregarCarrito">Agregar al carrito</button>
	</div>	

</div>

</div>

<div class="titulod">
	<h2>Productos similares</h2>

<div class="productosSimilares">
<?php include("includes/productosSimilares.php"); ?>
</div>
</div>
 <?php }}
 else {
	echo '<p class="noprod" >Producto no disponible</p>';
 }
?>
<?php include("includes/footer.html"); ?>
<script type="text/javascript" src="js/prod.js"></script>
<script src="js/main.js"></script>


</body>
</html>