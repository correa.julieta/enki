<!DOCTYPE html>
<html>
<head><link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="fonts/style.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet"  href="css/footer.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<link rel="stylesheet" type="text/css" href="css/caa.css">
	<title></title>
</head>
<body>
<div class="contenedor">
		<?php 
		include("includes/inicio.php");
			 include("includes/main.html");
			 include("backend/filtrosCategorias.php") ;
			?>
				<section>
		<article class="filtros">
			<div class="filtro">
				<a href="#" class="filtrar"><span class="icon-equalizer"></span>Filtrar</a>
				<!--<h2>{Nombre_producto}</h2><p class="resultados">{cantidad de resultados}</p>-->
				<div class="itemsF">
					
					
						<h3>Precio</h3>
						
							<p><input type="checkbox">Hasta $500</p>
							<p><input type="checkbox">Entre $500 y $1000</p>
							<p><input type="checkbox">Mas de $1000</p>
				
					
							<h3>Costo de envío</h3>
						
							<p><input type="checkbox">Gratis</p>
							<p><input type="checkbox">Sin envío</p></p>
					
				
</div>
			</div>
		</article>
 <article class="productos">
<?php 
			 			 if (mysqli_num_rows($filtros)>0) {
 	while ($registroF=mysqli_fetch_assoc($filtros)) {
				?>
		 <div class="producto">
			 		<div class="img">
		 
				<?php
				if (empty($registroF['foto'])) {
				 	echo '<img class="imgProd" src="imagenes/descarga.png">';
				 } else{
				 	echo '<img  class="imgProd" src="imagenes/'.$registroF['foto'].'">';
				 }
				 ?></div>
			<div class="p">	
		 <h2><?php echo $registroF['nbr_prod']; ?></h2>
			<p >[estrellas?]</p>
			<p class="items" >$ <?php echo $registroF['precio'];?> </p>
			<?php echo'<a href="producto.php?id_producto='.$registroF['id_producto'].'" class="verMas" >Ver más</a>' ;?>
			 		

			 	 </div></div>
			<?php } } else {
			 	 	echo '<p class="noprod" >No hay productos en esta categoria</p>';
			 	 }?>


</article></section>
			 	
			 
			<?php include("includes/footer.html"); ?>
</div>
<script src="js/main.js"></script>
</body>
</html>